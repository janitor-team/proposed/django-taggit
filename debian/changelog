django-taggit (0.24.0-3) UNRELEASED; urgency=medium

  * Bump Standards-Version to 4.4.1.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Ondřej Nový <onovy@debian.org>  Fri, 18 Oct 2019 16:02:44 +0200

django-taggit (0.24.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Thomas Goirand ]
  * Team upload.
  * Ran wrap-and-sort -bast.
  * Removed now useless Build-Conflicts:.
  * Removed Python 2 support.

 -- Thomas Goirand <zigo@debian.org>  Fri, 26 Jul 2019 15:43:27 +0200

django-taggit (0.24.0-1) unstable; urgency=medium

  [ Michal Čihař ]
  * New upstream release.
  * Bump standards to 4.3.0.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/watch: Use https protocol
  * d/control: Remove trailing whitespaces
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field

 -- Michal Čihař <nijel@debian.org>  Wed, 20 Feb 2019 10:50:01 +0100

django-taggit (0.22.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump standards to 4.0.0.

 -- Michal Čihař <nijel@debian.org>  Mon, 10 Jul 2017 19:57:28 +0200

django-taggit (0.21.3-1) unstable; urgency=medium

  * New upstream release.
  * Change VCS-Git URL to https.

 -- Michal Čihař <nijel@debian.org>  Thu, 17 Nov 2016 11:20:57 +0100

django-taggit (0.21.2-1) unstable; urgency=medium

  * New upstream release.
    - Fixes test on Django 1.10 (Closes: #834668)
  * Add Build-Conflicts with Django 1.10 as it breaks build (1.10.1 works
    fine).

 -- Michal Čihař <nijel@debian.org>  Wed, 07 Sep 2016 13:05:54 +0200

django-taggit (0.20.2-1) unstable; urgency=medium

  * New upstream release.
  * Move package to python modules git and team maintenance.

 -- Michal Čihař <nijel@debian.org>  Mon, 01 Aug 2016 11:42:29 +0200

django-taggit (0.20.1-1) unstable; urgency=medium

  * New upstream release.
    - Fixes build with Django 1.10 (Closes: #828659).

 -- Michal Čihař <nijel@debian.org>  Wed, 29 Jun 2016 15:18:45 +0200

django-taggit (0.20.0-1) unstable; urgency=medium

  * New upstream release.

 -- Michal Čihař <nijel@debian.org>  Tue, 21 Jun 2016 15:12:47 +0200

django-taggit (0.19.1-1) unstable; urgency=medium

  * New upstream release.

 -- Michal Čihař <nijel@debian.org>  Thu, 26 May 2016 09:15:27 +0200

django-taggit (0.18.3-1) unstable; urgency=medium

  * New upstream release.

 -- Michal Čihař <nijel@debian.org>  Wed, 18 May 2016 13:13:36 +0200

django-taggit (0.18.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump standards to 3.9.8.

 -- Michal Čihař <nijel@debian.org>  Thu, 12 May 2016 09:29:19 +0200

django-taggit (0.18.0-1) unstable; urgency=medium

  * New upstream release.

 -- Michal Čihař <nijel@debian.org>  Mon, 25 Jan 2016 15:46:09 +0100

django-taggit (0.17.6-1) unstable; urgency=medium

  * New upstream release.

 -- Michal Čihař <nijel@debian.org>  Mon, 14 Dec 2015 08:04:59 +0100

django-taggit (0.17.5-1) unstable; urgency=medium

  * New upstream release.
    - Django 1.9 compatibility.

 -- Michal Čihař <nijel@debian.org>  Fri, 04 Dec 2015 16:53:08 +0100

django-taggit (0.17.4-1) unstable; urgency=medium

  * New upstream release.

 -- Michal Čihař <nijel@debian.org>  Fri, 27 Nov 2015 08:37:05 +0100

django-taggit (0.17.3-1) unstable; urgency=medium

  * New upstream release.
    - Django 1.9 compatibility.

 -- Michal Čihař <nijel@debian.org>  Thu, 29 Oct 2015 14:44:36 +0100

django-taggit (0.17.1-1) unstable; urgency=medium

  * New upstream release.

 -- Michal Čihař <nijel@debian.org>  Thu, 17 Sep 2015 09:47:25 +0200

django-taggit (0.17.0-1) unstable; urgency=medium

  * New upstream release.
  * Update debian/watch.
  * Add python 3 package.
  * Run tests during build.

 -- Michal Čihař <nijel@debian.org>  Mon, 24 Aug 2015 13:44:42 +0200

django-taggit (0.12.2-1) unstable; urgency=medium

  * New upstream release.
    - Fixed 1.7 migrations.
  * Bump standards to 3.9.6.

 -- Michal Čihař <nijel@debian.org>  Thu, 25 Sep 2014 09:20:46 +0200

django-taggit (0.12.1-1) unstable; urgency=medium

  * New upstream release.
    - Complete support for Django 1.7 (Closes: #755614).

 -- Michal Čihař <nijel@debian.org>  Mon, 11 Aug 2014 09:55:45 +0200

django-taggit (0.12-1) unstable; urgency=low

  * New upstream release.
    - Backwards incompatible: Support for Django 1.7 migrations. South users
      have to set SOUTH_MIGRATION_MODULES to use taggit.south_migrations
      for taggit.
    - Backwards incompatible: Django's new transaction handling is used on
      Django 1.6 and newer.
    - Backwards incompatible: Tag.save got changed to opportunistically
      try to save the tag and if that fails fall back to selecting existing
      similar tags and retry -- if that fails too an IntegrityError is
      raised by the database, your app will have to handle that.
    - Added Italian and Esperanto translations.

 -- Michal Čihař <nijel@debian.org>  Tue, 29 Apr 2014 10:05:29 +0200

django-taggit (0.11.2-2) unstable; urgency=medium

  * Bump standards to 3.9.5.
  * Really do not ship tests in module without namespace (Closes: #738210).

 -- Michal Čihař <nijel@debian.org>  Mon, 10 Feb 2014 09:36:44 +0100

django-taggit (0.11.2-1) unstable; urgency=medium

  * New upstream release.
  * Do not ship tests in module without namespace (Closes: #733729).

 -- Michal Čihař <nijel@debian.org>  Fri, 03 Jan 2014 10:20:12 +0100

django-taggit (0.11.1-1) unstable; urgency=low

  * New upstream release.
  * Bump standards to 3.9.5.

 -- Michal Čihař <nijel@debian.org>  Mon, 09 Dec 2013 14:00:12 +0100

django-taggit (0.10a1-3) unstable; urgency=low

  * Fixed Vcs-* fields in debian/control (Closes: #721524).

 -- Michal Čihař <nijel@debian.org>  Mon, 09 Sep 2013 09:36:36 +0200

django-taggit (0.10a1-2) unstable; urgency=low

  * Fix typo in package name.

 -- Michal Čihař <nijel@debian.org>  Thu, 15 Aug 2013 16:48:21 +0200

django-taggit (0.10a1-1) unstable; urgency=low

  * Initial release. (Closes: #719809)

 -- Michal Čihař <nijel@debian.org>  Thu, 15 Aug 2013 16:31:05 +0200
